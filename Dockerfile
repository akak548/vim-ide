# VIM IDE 

FROM ubuntu:18.04 

LABEL maintainer="akak548@gmail.com"

ARG major_version
ARG config_version
ARG vim=8.1.0504
ARG tmux=2.8
ARG lastpass=1.3.1
ARG golang=1.11.2

ENV VIM_VERSION $vim
ENV TMUX_VERSION $tmux
ENV LASTPASS_VERSION $lastpass
ENV GOLANG_VERSION $golang
ENV TERM xterm-256color
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LINES 58
ENV SHELL /bin/bash
ENV IMAGE_ID blerdstyle32/ide
ENV CONFIG_VERSION $config_version
ENV MAJOR_VERSION $major_version

RUN apt-get update 

# Installing dependancies
RUN apt-get -y install \
    software-properties-common \
    build-essential \ 
    git \
    cmake \
    python-dev \
    python-pip \
    python3-dev \
    python3.6 \
    python-setuptools \
    libncurses5-dev \
    libncursesw5-dev \
    # TMUX
    libevent-2.1-6 \ 
    libevent-dev \
    autotools-dev \
    automake \ 
    pkg-config \
    # Bash 
    dnsutils \
    # Lastpass
    libcurl4 \
    libcurl4-openssl-dev \
    libssl-dev \
    libxml2 \
    libxml2-dev \
    libssl1.1 \
    ca-certificates \
    xclip \
    # GCP CLI
    lsb-release \
    gnupg \
    curl \
    openssh-client


# Install Python Dependencies
RUN pip install dotbot

# Install Tmux
RUN mkdir /tmp/tmux-$TMUX_VERSION && git clone --branch $TMUX_VERSION https://github.com/tmux/tmux.git /tmp/tmux-$TMUX_VERSION 
WORKDIR /tmp/tmux-$TMUX_VERSION
RUN sh autogen.sh && ./configure && make && make install

# Install Vim
RUN mkdir /tmp/vim-$VIM_VERSION && git clone --branch v$VIM_VERSION https://github.com/vim/vim.git /tmp/vim-$VIM_VERSION
WORKDIR /tmp/vim-$VIM_VERSION
RUN ./configure --disable-gui \
    --disable-netbeans \
    --enable-multibyte \
    --enable-terminal \
    --enable-pythoninterp \
    --with-python3-command \ 
    && make && make install 

# Install Lastpass
RUN mkdir /tmp/lastpass-$LASTPASS_VERSION && git clone --branch v$LASTPASS_VERSION https://github.com/lastpass/lastpass-cli.git /tmp/lastpass-$LASTPASS_VERSION
WORKDIR /tmp/lastpass-$LASTPASS_VERSION
RUN make && make install

# Install GCloud SDK
RUN export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" && \
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && apt-get install google-cloud-sdk -y

# Install GOLANG
RUN add-apt-repository ppa:longsleep/golang-backports && apt-get update && apt-get install -y golang-go

# Install Vimide
LABEL CONFIG=$CONFIG_VERSION

COPY ./ /ide/

WORKDIR "/ide"

RUN dotbot -v -c dotbot.yaml

RUN git submodule update --init --recursive

WORKDIR "/root/.vim/pack/git-plugins/start/youcompleteme"
RUN python3 install.py --clang-completer --go-completer

# Image Clean up
RUN apt-get clean \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /tmp/*-*  \
    && apt-get -y remove git cmake python-pip

VOLUME /code
VOLUME /root/.tmux 
VOLUME /root/.config

WORKDIR /code

ENTRYPOINT ["/bin/bash"]
