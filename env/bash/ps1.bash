## PS1

# Bash Colors
Blue="\[\e[0;34m\]"
LBlue="\[\e[1;34m\]"
Green="\[\e[0;32m\]"
LGreen="\[\e[1;32m\]"
Cyan="\[\e[0;36m\]"
LCyan="\[\e[1;36m\]"
Red="\[\e[0;31m\]"
LRed="\[\e[1;31m\]"
Purple="\[\e[0;35m\]"
LPurple="\[\e[1;35m\]"
Brown="\[\e[0;33\]"
Yellow="\[\e[1;33m\]"
LGray="\[\e[0;37m\]"
White="\[\e[1;37m\]"
Orange="\[\e[1;96m\]"
LOrange="\[\e[0;96m\]"
CReset="\[\e[30;1m\]"

function ps1_prompt {
	case $IMAGE_MODE in
		'ide')
			image_color=$Blue
			;;
		'cli')
			image_color=$Cryan
			;;
		*)
			image_color=$LBlue
			;;
	esac

  image_mode="${image_color}$IMAGE_MODE${CReset}"
	echo "\n${CReset}($image_mode)\$${CReset}"

}

function ps1_default {
 
  image_id="${Orange}$IMAGE_ID${CReset}"
  image_version="${LOrange}${MAJOR_VERSION}.${CONFIG_VERSION}${CReset}"
   
	echo "\n${CReset}($image_id:$image_version)-->${CReset}"
}

function _prompt {
	_PS1=''
	
	_PS1+=$(ps1_default)
  _PS1+=$(ps1_prompt)

  _PS1+="\[\e[0m\]"
	export PS1=$_PS1
}

PROMPT_COMMAND=_prompt
