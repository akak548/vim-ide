nnoremap <silent> <F5> :!clear;python %<CR>

" Replace tab with spaces
au Filetype py setl et ts=4 sw=4

" ========== Appearances ============="
 au BufEnter *.py colorscheme sublimemonokai

" Automatically open/close Nerdtree
autocmd vimenter *.py NERDTree
autocmd StdinReadPre * let s:std_in=1

let g:pymode = 0
let g:pymode_warnings = 1
let g:pymode_indent = 1
let g:pymode_options_colorcolumn = 1
let g:pymode_lint = 1
let g:pymode_lint_checkers = ['pep8', 'pylint']
let g:pymode_lint_message = 1
let g:pymode_virtualenv = 1
let g:pymode_python = 'python'

let g:pymode_syntax = 1

let NERDTreeIgnore=['\.pyc$', '\~$']
