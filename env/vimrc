" ========== Vim Basic Settings =============" 
filetype plugin indent on
syntax on
set number                                  " always show line numbers
set autoindent                              " copy the indentation of previous line 
set smartindent				    " predicts next indentation
set ignorecase                              " ignore case when searching
set smartcase                               " ignore case if search pattern is all lowercase
set incsearch				    " search text after first letter
set hlsearch				    " search highlighting
set history=1000                	    " Store lots of :cmdline history
set showcmd                     	    " Show incomplete cmds down the bottom
set showmode                     	    " Show current mode down the bottom
set gcr=a:blinkon0                          " Disable cursor blink
set visualbell                              " No sounds
set autoread                                " Reload files changed outside vim
"set norelativenumber

set foldmethod=indent			    " Enable folding
set foldlevel=1
"set foldclose=all

colorscheme darkblue                        " Default theme

set cursorline
set showmatch

set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

let g:deoplete#enable_at_startup = 1

set backspace=indent,eol,start " backspace over everything in insert mode

" ========== Configurations ============="
source ~/.vim/configuration/nerdtree.vimrc

" Set leader key to ,
let mapleader = ";"
let g:mapleader = ";"

" ========== Key Mapping ============="

"split navigations
noremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Enable folding with the space bar
nnoremap <space> za

" move vertically by visual line
nnoremap j gj
nnoremap k gk

" highlight last inserted text
nnoremap gV `[v`]`]`

" edit vimrc/zshrc and load vimrc bindings
nnoremap <leader>ev :vsp $MYVIMRC<CR>
nnoremap <leader>ez :vsp ~/.zshrc<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>

" ========== Abbreviation ============="
abbrev @@p	akak548@gmail.com

set encoding=utf8
