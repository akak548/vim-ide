MAJOR_VERSION=1.2.5
CONFIG_VERSION=3
IMAGE_VERSION=$(MAJOR_VERSION).$(CONFIG_VERSION)

build:
	docker build --build-arg config_version=$(CONFIG_VERSION) --build-arg major_version=$(MAJOR_VERSION) -t blerdstyle32/ide:$(IMAGE_VERSION) -t blerdstyle32/ide:latest .

push:
	docker push blerdstyle32/ide:$(IMAGE_VERSION)
	docker push blerdstyle32/ide:latest

